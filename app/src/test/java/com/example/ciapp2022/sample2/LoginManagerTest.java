package com.example.ciapp2022.sample2;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;


import org.junit.Before;
import org.junit.Test;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password");
    }
    @Test
    public void testSignupPassSuccess() throws ValidateFailedException, InvalidPasswordException, UserNotFoundException {
        loginManager.register("Testuser2","Normalpa55");
        User user = loginManager.login("Testuser2","Normalpa55");
        assertThat(user.getUsername(),is("Testuser2"));
        assertThat(user.getPassword(),is("Normalpa55"));
    }
    @Test(expected = ValidateFailedException.class)
    public void testSignupPassFailU() throws ValidateFailedException {
        loginManager.register("Testuser3","Nor");
    }
    @Test(expected = ValidateFailedException.class)
    public void testSignupPassFailD() throws ValidateFailedException {
        loginManager.register("Testuser4","Nor3!!!!");
    }
    @Test(expected = ValidateFailedException.class)
    public void testSignupPassFailT() throws ValidateFailedException {
        loginManager.register("Testuser5","normal3");
    }

    public void testLoginSuccess() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Testuser1", "Password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password"));
    }

    @Test(expected = ValidateFailedException.class)
    public void testLoginUpperCase() throws ValidateFailedException{
        loginManager.register("testuser2", "password");
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("iniad", "password");
    }
}